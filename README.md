# Test
# Hobbie(s) Adder

Hobbies adder is a simple webpage where users can select their hobbie(s).
## Usage

- Existing users can login through the login page.
- After logging in,it takes the user to a simple hobbie(s) adder page where users can select multiple hobbies from the list and save it.
- Newly signed user can easily sign up at the bottom part of the login form.
- after signing up it takes the same process for adding hobbies.

## Technologies Used
- Html/css
- CoreUi.io
- Local storage
- javascript.
- heroku


## Contributing
Pull requests are welcome. For major changes,error fixing,adding new features are welcome please open an issue first to discuss what you would like to change,add,fix.




